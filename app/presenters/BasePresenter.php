<?php
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Presenter;
use Nette\Application\UI\Form;
/**
 *
 */
abstract class BasePresenter extends Presenter
{
 public function beforeRender()
{
  parent::beforeRender();
  $this->template->menuItems=[
  'About'=>'Homepage:',
  'Registration'=>'Register:',
  'Example2'=>'Example2'

  ];
}
protected function createComponentContactForm(){
$form=new Form;
$form->addText('name','')->addRule(Form::FILLED,'Zadejte jméno')->setOption('class','l')
->setAttribute('placeholder', 'JMÉNO A PŘÍJMENÍ*');
$form->addText('email','')->addRule(Form::FILLED,'Zadejte email')->setAttribute('placeholder', 'EMAIL*')
     ->addRule(Form::EMAIL,"Email nemá žádný formát")->setOption('class','l2');
$form->addText('phone','')->addRule(Form::FILLED,'Zadejte email')->setOption('class','l2')
->setAttribute('placeholder', 'TELEFON');
$form->addTextArea('message','')->addRule(Form::FILLED,'Zadejte zprávu')->setOption('class','l')
->setAttribute('placeholder', 'ZPRÁVA*');
$form->addProtection('vypršel časový limit, odešlete formulář znovu');
$form->addSubmit('send','Odeslat');

$form->onSuccess[]=[$this,'proccessContactForm'];
return $form;
}

public function proccessContactForm(Form $form)
{
 $values=$form->getValues(TRUE);
 $message=new Message;
 $message->addTo('test@gmail.com')
         ->setForm($values['email'])
         ->setSubject('Zpráva z kontakního formuláře')
         ->setBody($values['message'])
         ->send();
 $this->flashMessage('Zpráva byla odeslána');
 $this->redirect('this');
}
}

 ?>
