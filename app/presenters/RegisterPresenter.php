<?php
/**
*
*/
namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class RegisterPresenter extends BasePresenter
{

  protected function createComponentRegistrationForm()
  {
    $form=new Form;
    $sex = [
      'm' => 'male',
      'f' => 'female',
    ];
    $yearCurrent=date("Y");
    $years= array();
    $in=0;
    $currentYear=date("Y");
    for ($i=$currentYear; $i >=1905; $i--) {

      $years[$in]=$i;
      $in++;
    }
    $form->addText('name','')->addRule(Form::FILLED,"name")->setAttribute('placeholder','Name')->setOption('class','l2');
    $form->addText('lastName','')->addRule(Form::FILLED,"lastname")->setAttribute('placeholder','lastname')->setOption('class','l2');
    $form->addText('email','')->addRule(Form::FILLED,'Zadejte email')->setAttribute('placeholder', 'EMAIL*')
    ->addRule(Form::EMAIL,"Email nemá žádný formát")->setOption('class','l');
    $form->addPassword('password','')->setRequired("Fill the password")->setOption('class','l')
    ->addRule(Form::PATTERN, 'Musí obsahovat číslici', '.*[0-9].*')
    ->addRule(Form::MIN_LENGTH, 'The password %d znaky', 8);
    $form->addPassword('passwordVerify', '')->setOption('class','l')
    ->setRequired('Zadejte prosím heslo ještě jednou pro kontrolu')
    ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password']);
    $form->addProtection('vypršel časový limit, odešlete formulář znovu');

    $form->addRadioList('gender', 'Gender:', $sex)
    ->getSeparatorPrototype()->setName(NULL);

    $form->  addSelect('Date', '',$years)->setPrompt('Year');;

    $form->addSubmit('send','Send');

    $form->onSuccess[]=[$this,'proccessContactForm'];
    return $form;
  }

}


?>
